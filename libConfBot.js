// ################################
// Library Conference Papers Bot ##
// ################################

// Create a simple server to keep the bot running
var http = require('http');
http.createServer(function (req, res) {
  res.writeHead(200, {'Content-Type': 'text/plain'});
  res.end('Library Conference Proposals Bot\n');
}).listen(8080);

// REQUIRE packages
 require('dotenv').load();
var random = require("random-js")();
var fs =require("fs");
var request = require("request");
var FeedParser = require ("feedparser");
var WordPOS = require('wordpos');
var Twit = require('twit');

// initiate wordpos
wordpos = new WordPOS();

// initiate simple-twitter
var T = new Twit({
	consumer_key: process.env.TWITTER_CONSUMER_KEY,
	consumer_secret: process.env.TWITTER_CONSUMER_SECRET,
	access_token: process.env.TWITTER_ACCESS_TOKEN,
	access_token_secret: process.env.TWITTER_ACCESS_TOKEN_SECRET
});

// Set timeout to loop the whole thing every 2.1 hours
var timerVar = setInterval (function () {writeAbstracts()}, 7.56e+6);

// Run the bot when the timer expires
function writeAbstracts() {

var sArray = []

	// set a Date variable for yesterday.
	var dateNow = new Date();
	var newest = dateNow - 8.64e+7;

	// generate a random value between 1 and however many lines there are in the phrases.txt file, minus one
	var intgr = random.integer(1, 60);
	// use the random value to pick a line from the phrases.txt file.
	var phrases = fs.readFileSync('phrases.txt').toString().split('\n');
	cliche = phrases[intgr];

	// Get Reuters tech news from their RSS feed
	var req = request("http://feeds.reuters.com/reuters/technologyNews");
	var feedparser = new FeedParser();

	// deal with any errors in FeedParser
	req.on('error', function (error) {
	  console.error(error); 
	});

	req.on('response', function (res) {
	  var stream = this;
	  // deal with any errors in the feed
	  if (res.statusCode != 200) return this.emit('error', new Error('Bad status code'));
	  stream.pipe(feedparser);
	});

	// deal with any errors in the code
	feedparser.on('error', function(error) {
	  console.error(error); 
	});

	feedparser.on('readable', function() {
	  // here we go... 
	  var stream = this
	      , meta = this.meta
	      , item;	

	  while (item = stream.read()) {
		t = item.title;

			// split the headline into an array of words
			var pHead = t.split(" ");

			// get the first two words and replace the comma with a space
			var newSubject = pHead.slice(0, 2);
			var newStr = newSubject.join(" ");

			// get the last word
			var sEnd = pHead.length;
			var sRest = sEnd - 1;
			var	lastWord = pHead.slice(sRest, sEnd);
			// make a sentence and push to sArray
			var mySentence = newStr + ' ' + lastWord + ': what it means for ' + cliche;			
			sArray.push(mySentence);

			// the feed will have 20 headlines, so once we've got that many we choose one at random
			if (sArray.length > 19) {
				var x = random.integer(0,19);
				var option1 = '\n' + sArray[x];

				// then we push it to the options.txt file
				fs.appendFileSync('options.txt', option1);
			}
		}
	});

	// generate some random nouns.
	var rNoun = wordpos.randNoun({count: 2}, function(rN){
		// define a variable to find underscores
		var cleanup = /_/g;
		// use it to replace underscores with spaces in each noun
		// we need to do this because wordpos creates them with underscores
		var noun1 = rN[0];
		var noun2 = rN[1];
		var clean1 = noun1.replace(cleanup, " ");
		var clean2 = noun2.replace(cleanup, " ");

		// randomly choose one of the top ten trending topics from Austrailan Twitter
		// remember if you're testing this that you can only hit the REST API 15 times
		// each 15 minutes (i.e. once every 60000 milliseconds)
		var rTrend = random.integer(1,11);
		var rT = rTrend - 1;
		T.get('trends/place', {id:'23424748'}, function(err, data, response){
			var trends = data[0];
			var tTopic = trends.trends[rT].name;
			// add a second sentence to options.txt
			var Option2 = '\n' + "How " + tTopic + " can tranform " + cliche + ".";
			fs.appendFileSync('options.txt', Option2);
		});

		// append the final two sentences to options.txt
		var Option3 = '\n' + "Why " + clean1 + " could be the " + clean2 + " of libraries.";
		fs.appendFileSync('options.txt', Option3);
		var Option4 = '\n' + "Is " + clean1 + " the future of libraries?";
		fs.appendFileSync('options.txt', Option4);

	});

	// generate a random value between 1 and however many lines there are in the phrases.txt file
	var k = random.integer(1, 4);
	// use the random value to pick one of the lines we created in the phrases.txt file.
	var options = fs.readFileSync('options.txt').toString().split('\n');
	tweet = options[k];

	// tweet the title!
	T.post('statuses/update', {status: tweet}, function(err, data, response){
		if (err) {
			console.log(err);
		}
	})

	// record when it looped to help with troubleshooting if needed
	var loopDate = new Date();
	console.log('looped at ' + loopDate);

	// clear out the file for the next run.
	fs.writeFileSync('options.txt', " ");
}