zero
3D printing in libraries
gamification
linked open data
RFID self service
professional certification
makerspaces
de-professionalisation
Second Life
the dominance of Google
Wikipedia in libraries
#OpenAccess publishing
Elsevier
@EBSCO
@OCLC
Chromebooks
iPad vending machines
Lego clubs
BIBFRAME
Schema.org integration
the future of MARC
Inter-Library Loans
Institutional Repositories
Community-led librarianship
Radical Librarians
the rise of ebooks
the Serials Crisis
Academic Presses
Free Little Libraries
the card catalogue
the Dewey Decimal System
information overload
librarian stereotypes
roving reference
Design Thinking
three-column libguides
innovation
collaboration
embracing new technology
this era of rapid change
library closures
elevator pitches
drone book delivery
academic tenure for librarians
MOOCs
the Internet of Things
tattooed librarians
thinking like a startup
maker culture
one-shot instruction
digital natives
patron-driven acquisitions
the MLIS
@PLOSOne
WiFi speeds
getting rid of the OPAC
passwords on post-it notes
patron privacy
passive-aggressive signage
@DOAJplus
censorship